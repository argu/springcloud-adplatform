package cn.dota2info.elk.controller;

import cn.dota2info.elk.Service.BookSearchService;
import cn.dota2info.elk.Service.BookService;
import cn.dota2info.elk.entity.BaseSearchParam;
import cn.dota2info.elk.entity.Book;
import com.wwmxd.common.msg.ListRestResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("book")
@Slf4j
public class BookController {
    @Autowired
    BookService service;
    @Autowired
    BookSearchService searchService;
    @RequestMapping("insert")
    public Mono<String> insertBook(@RequestBody Book book){
        return Mono.create(sink->sink.success(service.insert(book)));
    }

    @RequestMapping(value = "getBook/{id}",method = RequestMethod.GET)
    public Mono<Book> getBook(@PathVariable(value = "id") Integer id){
      return Mono.create(sink->sink.success(service.get(id)));
    }
    @RequestMapping(value = "deleteBook",method = RequestMethod.POST)
    public Mono<Boolean> deleteBook(@RequestBody String id){
        return Mono.create(sink->sink.success(service.delete(id)));
    }

    @RequestMapping("update")
    public Mono<Long> updateBook(@RequestBody Book book){
        return Mono.create(sink->sink.success(service.update(book)));
    }

    @RequestMapping("getAll")
    public Mono<ListRestResponse> getAllBook(@RequestBody BaseSearchParam param){
        return Mono.create(sink -> {
            try {
                sink.success(service.getAllBook(param));
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }


}
