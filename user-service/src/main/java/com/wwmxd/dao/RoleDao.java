package com.wwmxd.dao;

import com.wwmxd.entity.Role;
import com.wwmxd.common.mapper.SuperMapper;

/**
 *
 * 
 *
 * @author WWMXD
 * @email 309980030@qq.com
 * @date 2018-01-03 14:39:38
 */
public interface RoleDao extends SuperMapper<Role> {

}